## About The Apps

How to run:

- run composer install.
- run npm install.
- make file .env and copy file .env.example to .env.
- make database and name it based on .env.
- run php artisan migrate or fresh --seed.
- run php artisan serve.
- admin account can be viewed on table users, the password is password of all account.
- if the email not send, check spam
