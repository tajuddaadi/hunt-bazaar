<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Invitation;
use App\Models\Status;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $keyword = $request->input('keyword');

        $invitations = Invitation::with('status')->where(function ($query) use ($keyword) {
            if (isset($keyword)) {
                $query->where('email', 'LIKE', '%' . $keyword . '%');
            }
        })->latest()->paginate(10);

        return view('admin.invitations.index', compact('invitations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.invitations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send($generatedLink, $realLink, $email)
    {
        $details = [
            'title' => 'Event Hunt Bazaar December 12th 2021',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit deserunt laborum dolores at officia blanditiis commodi cupiditate nobis, totam quibusdam ullam numquam hic dolorem fugiat exercitationem consectetur, quia praesentium quas!',
            'link' => $generatedLink,
            'realLink' => $realLink,
        ];

        try {
            \Mail::to($email)->send(new \App\Mail\InvitationMail($details));
            echo "Success.";
        } catch (\Exception $e) {
            echo "Failed $e.";
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
        ], [
            'email.required' => 'Please input email',
            'email.emmail' => 'Please input correct format',
        ]);

        $randomString = Str::random(5);
        $link = 'huntbazaar.com/';
        $status = Status::where('slug', 'send')->first();

        try {
            $invitation = new Invitation();
            $invitation->status_id = $status->id;
            $invitation->email = $request->email;
            $invitation->invitation_token = $randomString;
            $invitation->invitation_link = $link . $randomString;
            $invitation->save();

            $invitationLink = Invitation::find($invitation->id);
            $invitationLink->real_link = route('entry.show', $invitation->id);
            $invitationLink->save();

            $this->send($invitation->invitation_link, $invitationLink->real_link, $invitation->email);
        } catch (\Throwable $th) {
            $error = [
                'class' => 'alert-danger',
                'message' => "Failed to send email",
            ];

            return back()->with($error);
        }

        $status = [
            'class' => 'alert-success',
            'message' => "Success Send Email",
        ];

        return redirect()->route('admin.invitation.index')->with($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}