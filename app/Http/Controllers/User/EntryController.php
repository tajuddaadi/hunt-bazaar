<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Designer;
use App\Models\Entry;
use App\Models\Invitation;
use App\Models\Status;
use Illuminate\Http\Request;

class EntryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send($token, $email)
    {
        $details = [
            'title' => 'Thank You for Being Involved in Event Hunt Bazaar December 12th 2021',
            'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Odit deserunt laborum dolores at officia blanditiis commodi cupiditate nobis, totam quibusdam ullam numquam hic dolorem fugiat exercitationem consectetur, quia praesentium quas!',
            'token' => $token,
        ];

        try {
            \Mail::to($email)->delay(360)->send(new \App\Mail\ThanksMail($details));
            echo "Success.";
        } catch (\Exception $e) {
            echo "Failed $e.";
        }
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'birth_date' => 'required',
            'gender' => 'required',
            'designer' => 'required',
        ], [
            'email.required' => 'Please input email',
            'email.email' => 'Please input correct format',
            'birth_date.required' => 'Please input birth date',
            'gender.required' => 'Please input gender',
            'designer.required' => 'Please input designer',
        ]);

        $status = Status::where('slug', 'registered')->first();

        try {
            $entry = new Entry();
            $entry->invitation_id = $request->invitation_id;
            $entry->email = $request->email;
            $entry->date_of_birth = $request->birth_date;
            $entry->gender = $request->gender;
            $entry->favorite_designer = $request->designer;
            $entry->save();

            $invitation = Invitation::find($request->invitation_id);
            $invitation->status_id = $status->id;
            $invitation->save();

            $this->send($invitation->invitation_token, $invitation->email);
        } catch (\Throwable $th) {
            $error = [
                'class' => 'alert-danger',
                'message' => "Failed to register",
            ];

            return back()->with($error);
        }

        $status = [
            'class' => 'alert-success',
            'message' => "Success register",
        ];

        return back()->with($status);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $invitation = Invitation::find($id);
        $checkInvitation = Entry::where('invitation_id', $invitation->id)->exists();
        $designers = Designer::get();

        return view('user.entries.show', compact('invitation', 'checkInvitation', 'designers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function loadDataDesigner(Request $request)
    {
        $data = [];

        if ($request->has('q')) {
            $search = $request->q;
            $data = Designer::select('id', 'name')
                ->where('name', 'LIKE', '%' . $search . '%')
                ->get();
        }

        return response()->json($data);
    }
}