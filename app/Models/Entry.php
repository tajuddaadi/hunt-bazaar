<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Entry extends Model
{
    use HasFactory;

    protected $casts = [
        'favorite_designer' => 'array'
    ];

    public function invitation()
    {
        return $this->belongsTo(Invitation::class);
    }
}