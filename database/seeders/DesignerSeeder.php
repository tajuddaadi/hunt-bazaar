<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Designer;

class DesignerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function addData($designers)
    {
        foreach ($designers as $designer) {
            $hasdesignerData = Designer::where('name', $designer['name'])->first();
            if (!$hasdesignerData) {
                Designer::create([
                    'name' => $designer['name'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }

    public function run()
    {
        $designers = array(
            array(
                'name' => '3.1 Phillip Lim',
            ), array(
                'name' => '7 For All Mankind',
            ), array(
                'name' => 'A Bathing Ape',
            ), array(
                'name' => 'A.L.C',
            ), array(
                'name' => 'A.W.A.K.E',
            ), array(
                'name' => 'Acqua Di Parma',
            ), array(
                'name' => 'Adam Lippes',
            ), array(
                'name' => 'Adidas',
            ), array(
                'name' => 'Adidas by Y-3 Yohji Yamamoto',
            ), array(
                'name' => 'Air Jordan',
            ), array(
                'name' => 'Alexis',
            ), array(
                'name' => 'Amiri',
            ), array(
                'name' => 'Armani Baby',
            ), array(
                'name' => 'B By Brian Atwood',
            ), array(
                'name' => 'Baby Dior',
            ), array(
                'name' => 'Bailey',
            ), array(
                'name' => 'Balenciaga',
            ), array(
                'name' => 'BAPE Kids',
            ), array(
                'name' => 'Bates',
            ), array(
                'name' => 'Bella Freud',
            ), array(
                'name' => 'Blugirl Blumarine',
            ), array(
                'name' => 'Bvlgari',
            ), array(
                'name' => 'Cacharel',
            ), array(
                'name' => 'Calvin Klein',
            ), array(
                'name' => 'Canali',
            ), array(
                'name' => 'Céline',
            ),

        );

        $this->addData($designers);
    }
}