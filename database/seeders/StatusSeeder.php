<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use App\Models\Status;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function addData($statuses)
    {
        foreach ($statuses as $status) {
            $hasStatusData = Status::where('name', $status['name'])->first();
            if (!$hasStatusData) {
                Status::create([
                    'name' => $status['name'],
                    'slug' => $status['slug'],
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }

    public function run()
    {
        $statuses = array(
            array(
                'name' => 'Send by Admin',
                'slug' => 'send',
            ),
            array(
                'name' => 'Registered by User',
                'slug' => 'registered',
            ),
        );

        $this->addData($statuses);
    }
}