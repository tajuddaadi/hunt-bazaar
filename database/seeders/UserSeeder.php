<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    const ROLE_ADMIN = 'admin';

    public function addData($users, $role)
    {
        foreach ($users as $user) {
            $hasUserData = User::where('name', $user['name'])->first();
            $password = Hash::make('password');
            if (!$hasUserData) {
                User::create([
                    'name' => $user['name'],
                    'email' => $user['email'],
                    'password' => $password,
                    'role' => $role,
                    'email_verified_at' => Carbon::now(),
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                ]);
            }
        }
    }

    public function run()
    {
        $admins = array(
            array(
                'name' => 'Budi Benny Kartawijaya',
                'email' => 'admin@gmail.com',
            ),
            array(
                'name' => 'Dian Teguh Indrajaya',
                'email' => 'admin2@gmail.com',
            ),
        );

        $this->addData($admins, self::ROLE_ADMIN);

        User::factory()->count(10)->create();
    }
}