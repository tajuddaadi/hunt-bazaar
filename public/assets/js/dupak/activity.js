$(function() {
    $('.select2').select2();

    getUserSelection();

    $('[type="date"]').prop('max', function() {
        return new Date().toJSON().split('T')[0];
    });

    $('[type="date"]').prop('min', function() {
        let min_date = new Date().setFullYear(new Date().getFullYear() - 2);
        return new Date(min_date).toJSON().split('T')[0];
    });

    $('#how-to').on('change', function(e) {
        let val = $(this).val();
        if (val == 2) {
            enableGroupWork();
        }else if (val == 3) {
            enableTeamWork();
        } else {
            enableSingleWork();
        }
    });

    $('#btn_internal_number').on('click', function(e) {
        cloneInternalUser();
    });

    $('#btn_external_number').on('click', function(e) {
        cloneExternalUser();
    });

    $('#btn_internal_number2').on('click', function(e) {
        cloneInternalUser2();
    });

    $('#btn_external_number2').on('click', function(e) {
        cloneExternalUser2();
    });

    $('#datepicker').datepicker({
        dateFormat: 'dd-mm-yy',
        maxDate: 0,
        minDate: '-2Y',
    });

    $('.change_doc').on('click', function(e) {
        e.preventDefault();
        let el = $(this);
        let file_drop_area_el = el.parent().parent().parent().find('.file-drop-area');
        file_drop_area_el.removeClass('d-none');

        if (el.data('is-opened') == false) {
            file_drop_area_el.removeClass('d-none');
            el.data('isOpened', true);
        } else {
            file_drop_area_el.addClass('d-none');
            el.data('isOpened', false);
        }
    });

    document.querySelector("#credit-score").addEventListener("keypress", function (e) {
        var allowedChars = '0123456789,';
        function contains(stringValue, charValue) {
            return stringValue.indexOf(charValue) > -1;
        }
        var invalidKey = e.key.length === 1 && !contains(allowedChars, e.key)
                || e.key === '.' && contains(e.target.value, '.');
        invalidKey && e.preventDefault();});
});

function selectResultUnit() {
    var result = $("#result_unit_id").val();
    $.ajax({
            method: "POST",
            url: "/api/get-production-type",
            data: {
                id: result,
            }
        })
        .done(function(results) {
            setProduction(results);
            if(result == 34 || result == 35 || result == 36){
                $('#validation-file').val(1);
                $('#upload-file-1').addClass('d-none');
                $('#upload-file-1').find('input').attr('disabled', 'disabled');
                $('#upload-file-2').addClass('d-none');
                $('#upload-file-2').find('input').attr('disabled', 'disabled');
                $('#upload-file-3').addClass('d-none');
                $('#upload-file-3').find('input').attr('disabled', 'disabled');
            } else {
                $('#validation-file').val(2)
                $('#upload-file-1').removeClass('d-none');
                $('#upload-file-1').find('input').removeAttr('disabled', 'disabled');
                $('#upload-file-2').removeClass('d-none');
                $('#upload-file-2').find('input').removeAttr('disabled', 'disabled');
                $('#upload-file-3').removeClass('d-none');
                $('#upload-file-3').find('input').removeAttr('disabled', 'disabled');
            }
        }).fail(function(xhr, status, error) {
            //Ajax request failed
            var errorMessage = xhr.status + ': ' + xhr.statusText
            swal("Error", "Error - " + errorMessage, "error");
        });
}

function setProduction(productionTypes) {
    $("#production_type_id").empty();
    $("<option selected disabled>Pilih Hasil Kerja</option>")
        .appendTo("#production_type_id");
    productionTypes.forEach(function(production) {
        $("<option>", {
                value: production.id,
                text: production.title,
            })
            .appendTo("#production_type_id")
    });
}

function selectLabel() {
    var result = $("#production_type_id").val();
    $.ajax({
            method: "POST",
            url: "/api/get-label",
            data: {
                production_type_id: result,
            }
        })
        .done(function(results) {
            setTitle(results);
        }).fail(function(xhr, status, error) {
            //Ajax request failed
            var errorMessage = xhr.status + ': ' + xhr.statusText
            swal("Error", "Error - " + errorMessage, "error");
        });
}

function setLabel(results, label) {
    $(label).empty();
    results.forEach(function(result) {
        $("<span>", {
            text: result.title,
        }).appendTo(label)
    });
}

function setTitle(results) {
    var title1 = "#coba-label";
    var title2 = "#coba-label-2";
    setLabel(results, title1);
    setLabel(results, title2);
}

function enableSingleWork() {
    $('#group_work').addClass('d-none');
    $('#group_work').find('input').attr('disabled', 'disabled');
    $('#group_work').find('select').attr('disabled', 'disabled');
    $('#single_work').find('input').removeAttr('disabled', 'disabled');
}

function enableGroupWork() {
    $('#group_work').removeClass('d-none');
    $('#group_work').find('input').removeAttr('disabled', 'disabled');
    $('#group_work').find('select').removeAttr('disabled', 'disabled');
    $('#as_group').removeClass('d-none');
    $('#as_group').find('input').removeAttr('disabled', 'disabled');
    $('#as_group').find('select').removeAttr('disabled', 'disabled');
    $('#as_team').addClass('d-none');
    $('#as_team').find('input').attr('disabled', 'disabled');
    $('#as_team').find('select').attr('disabled', 'disabled');
    $('#single_work').find('input').attr('disabled', 'disabled');
}

function enableTeamWork() {
    $('#group_work').removeClass('d-none');
    $('#group_work').find('input').removeAttr('disabled', 'disabled');
    $('#group_work').find('select').removeAttr('disabled', 'disabled');
    $('#as_team').removeClass('d-none');
    $('#as_team').find('input').removeAttr('disabled', 'disabled');
    $('#as_team').find('select').removeAttr('disabled', 'disabled');
    $('#as_group').addClass('d-none');
    $('#as_group').find('input').attr('disabled', 'disabled');
    $('#as_group').find('select').attr('disabled', 'disabled');
    $('#single_work').find('input').attr('disabled', 'disabled');
}

function getUserSelection() {
    let internal = $('#wrapper_internal_creator').find('select[name="internal_creator[]"]').val();
    $('select[name="internal_creator[]"]').select2({
        width: 'resolve',
        ajax: {
            url: "/api/activity-user",
            dataType: 'json',
            delay: 250,
            processResults: function(data) {
                return {
                    results: $.map(data, function(item) {
                        if(internal != item.id){
                            let nip = item.profile ? item.profile.nip : '-';
                            return {
                                text: item.name + ' (NIP : ' + nip + ')',
                                id: item.id
                            }
                        }
                    })
                };
            },
            cache: true
        },
        minimumInputLength: 1,
        language: {
            inputTooShort: function() {
                return 'Silahkan masukkan minimal 1 karakter untuk mencari data';
            }
        }
    });

    $('select[name="internal_creator_as[]"]').select2();
}

function cloneInternalUser() {
    let el = $('#internal_number');
    let wrapper = $('#wrapper_internal_creator');
    let row = wrapper.find('div.row').first();
    let children = wrapper.find('div.row').length;
    let remaining_el = el.val() - children;

    if (el.val() < 1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    if (children == 1 && remaining_el == -1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    row.find('select.select2').select2('destroy');

    if (remaining_el >= 0) {
        for (let index = 0; index < remaining_el; index++) {
            row.clone().appendTo(wrapper);
        }
    } else {
        for (let index = 0; index > remaining_el; index--) {
            wrapper.find('div.row').last().remove();
        }
    }

    getUserSelection();
}

function cloneExternalUser() {
    let el = $('#external_number');
    let wrapper = $('#wrapper_external_creator');
    let row = wrapper.find('div.row').first();
    let children = wrapper.find('div.row').length;
    let remaining_el = el.val() - children;

    if (el.val() < 1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    if (children == 1 && remaining_el == -1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    row.find('select.select2').select2('destroy');

    if (remaining_el >= 0) {
        for (let index = 0; index < remaining_el; index++) {
            row.clone().appendTo(wrapper);
        }
    } else {
        for (let index = 0; index > remaining_el; index--) {
            wrapper.find('div.row').last().remove();
        }
    }

    $('select[name="external_creator_as[]"]').select2();
}

function cloneInternalUser2() {
    let el = $('#internal_number2');
    let wrapper = $('#wrapper_internal_creator2');
    let row = wrapper.find('div.row').first();
    let children = wrapper.find('div.row').length;
    let remaining_el = el.val() - children;

    if (el.val() < 1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    if (children == 1 && remaining_el == -1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    row.find('select.select2').select2('destroy');

    if (remaining_el >= 0) {
        for (let index = 0; index < remaining_el; index++) {
            row.clone().appendTo(wrapper);
        }
    } else {
        for (let index = 0; index > remaining_el; index--) {
            wrapper.find('div.row').last().remove();
        }
    }

    getUserSelection();
}

function cloneExternalUser2() {
    let el = $('#external_number2');
    let wrapper = $('#wrapper_external_creator2');
    let row = wrapper.find('div.row').first();
    let children = wrapper.find('div.row').length;
    let remaining_el = el.val() - children;

    if (el.val() < 1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    if (children == 1 && remaining_el == -1) {
        swal("Peringatan!", "Jumlah anggota tidak boleh kurang dari 1", "error");

        return;
    }

    row.find('select.select2').select2('destroy');

    if (remaining_el >= 0) {
        for (let index = 0; index < remaining_el; index++) {
            row.clone().appendTo(wrapper);
        }
    } else {
        for (let index = 0; index > remaining_el; index--) {
            wrapper.find('div.row').last().remove();
        }
    }

    $('select[name="external_creator_as[]"]').select2();
}

function validateMemberAs(element) {
    let internalWrapper = $('#wrapper_internal_creator');
    let externalWrapper = $('#wrapper_external_creator');

    let countValue = [];

    internalWrapper.find('select[name="internal_creator_as[]"]').map(function() {
        countValue[this.value] = (countValue[this.value] || 0) + 1;
    });

    externalWrapper.find('select[name="external_creator_as[]"]').map(function() {
        countValue[this.value] = (countValue[this.value] || 0) + 1;
    });

    if (countValue[3] > 1) {
        swal("Peringatan", "Batas ketua tim/kelompok maksimal 1 orang", "error");
        $(element).val(null).trigger('change');
    }
}

function selectMaxScore(element) {
    var result = $("#production_type_id").val();
    $.ajax({
            method: "POST",
            url: "/api/get-label",
            data: {
                production_type_id: result,
            }
        })
        .done(function(results) {
            checkScore(results, element);
        }).fail(function(xhr, status, error) {
            //Ajax request failed
            var errorMessage = xhr.status + ': ' + xhr.statusText
            swal("Error", "Error - " + errorMessage, "error");
        });
}

function checkScore(results, element) {
    results.forEach(function(result) {
        var input = $(element).val();
        var number = input.replace(",", ".");
        if (parseFloat(number) > result.maximum_credit) {
            swal("Peringatan!", "Skor pengusulan melebihi skor maksimum kredit! " + result.maximum_credit.replace(".", ","), "error");
            $(element).val(1)
        }
    });
}
