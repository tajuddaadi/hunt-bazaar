@extends('layouts.master')

@section('css')

@endsection

@section('content')
<div class="container-scroller">
    @include('layouts.role.role-navbar')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        @include('layouts.role.role-sidebar')
        <div class="main-panel" style="overflow-y: scroll; height:450px;">

            <div class="content-wrapper">

                <div class="row">
                    <div class="col-md-12 stretch-card">
                        <div class="card">
                            <div class="card-body" style="min-height: 600px;">
                                <h4 class="card-title">Add Invitation</h4>
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (session('message'))
                                <div class="alert {{ session('class') }}">
                                    {{ session('message') }}
                                </div>
                                @endif
                                <p class="card-description">
                                    Form send invitation via email
                                </p>
                                <form class="forms-sample" method="POST" action="{{ route('admin.invitation.store') }}"
                                    enctype="multipart/form-data">
                                    @csrf
                                    {!!Form::text('email', 'Email')
                                    ->placeholder('Input Email')->type('email')->required() !!}
                                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                                    <button class="btn btn-light">Cancel</button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->

            </div> <!-- content-wrapper ends -->

            @endsection

            @section('js')
            @endsection