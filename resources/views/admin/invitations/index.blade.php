@extends('layouts.master')

@section('css')

@endsection

@section('content')
<div class="container-scroller">
    @include('layouts.role.role-navbar')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        @include('layouts.role.role-sidebar')
        <div class="main-panel" style="overflow-y: scroll; height:450px;">

            <div class="content-wrapper">

                <div class="row">
                    <div class="col-md-12 stretch-card">
                        <div class="card">
                            <div class="card-body" style="min-height: 600px;">
                                <p class="card-title">Data Invitations</p>
                                <p class="card-description">
                                    @if (session('message'))
                                <div class="alert {{ session('class') }}">
                                    {{ session('message') }}
                                </div>
                                @endif
                                </p>
                                <div class="row justify-content-between mb-3">
                                    <div class="col-md-4">
                                        <a href="{{ route('admin.invitation.create') }}" type="button"
                                            class="btn btn-success btn-sm">Input data Invitation</a>
                                    </div>
                                    <div class="col-md-4">
                                        <form method="GET" class="form-inline"
                                            action="{{ route('admin.invitation.index') }}">
                                            <input type="text" class="form-control form-control-sm" name="keyword"
                                                placeholder="Search email .." value="{{ old('keyword') }}">
                                            <button type="submit" class="btn btn-primary btn-sm">Cari</button>
                                        </form>
                                    </div>

                                </div>


                                <div class="table-responsive">
                                    <table id="alumnii" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Email</th>
                                                <th>Status</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($invitations as $invitation)
                                            <tr>
                                                <td>{{ $invitation->email }}</td>
                                                <td>{{ $invitation->status->name }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    {{ $invitations->appends(['keyword' => old('keyword') ])->links() }}
                                </div>
                            </div>

                        </div>
                    </div> <!-- row -->

                </div> <!-- content-wrapper ends -->
                @endsection

                @section('js')

                @endsection