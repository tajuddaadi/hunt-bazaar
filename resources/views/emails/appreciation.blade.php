<!DOCTYPE html>
<html>

<head>
    <title>Thank you</title>
</head>

<body>
    <h1>{{ $details['title'] }}</h1>
    <p>{{ $details['body'] }}</p>

    <p>Here is your invitation code : </p>
    <p>{{ $details['token'] }}</p>
</body>

</html>