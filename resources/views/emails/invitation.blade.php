<!DOCTYPE html>
<html>

<head>
    <title>You have an invitation event from Hunt Bazaar</title>
</head>

<body>
    <h1>{{ $details['title'] }}</h1>
    <p>{{ $details['body'] }}</p>

    <p>You can register by following this link </p>
    <a href="{{ $details['realLink'] }}">{{ $details['link'] }}</a>
</body>

</html>