<!-- Footer Start -->
<div class="flex-grow-1"></div>
<div class="app-footer">
    <div class="footer-bottom border-top pt-3 d-flex flex-column flex-sm-row align-items-center">
        <div class="d-flex align-items-center">
            <img class="logo" src="{{asset('assets/images/logo.png')}}" alt="">
            <div>
                <p class="m-0">&copy; {{ date('Y') }} Konnco Studio</p>
                <p class="m-0">Theme by Gull</p>
                <p class="m-0">All rights reserved</p>
            </div>
        </div>
    </div>
</div>
<!-- fotter end -->
