<div class="main-header">
    <div class="logo">
        <img src="{{asset('assets/images/logo.png')}}" alt="">
    </div>

    <div class="menu-toggle">
        <div></div>
        <div></div>
        <div></div>
    </div>

    <div class="d-flex align-items-center">
        <div class="search-bar">
            <form action="{{route('admin.generated-url.index')}}">
            <input name="keyword_url" id="keyword_url" type="text" value="{{app('request')->input('keyword_url')}}" placeholder="Enter actual url or masked url">
            <i class="search-icon text-muted i-Magnifi-Glass1"></i>
            </form>
        </div>
    </div>

    <div style="margin: auto"></div>

    <div class="header-part-right">
        <!-- Full screen toggle -->
        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
        <!-- Grid menu Dropdown -->

        <!-- User avatar dropdown -->
        <div class="dropdown">
            <div class="user col align-self-end">
                <img src="{{asset('assets/images/profile.png')}}" id="userDropdown" alt="" data-toggle="dropdown"
                    aria-haspopup="true" aria-expanded="false">

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                    <div class="dropdown-header">
                        <i class="i-Lock-User mr-1"></i>{{Auth::user()->name}}
                    </div>
                    <a class="dropdown-item" href="javascript: document.getElementById('logout').submit();">Logout</a>
                    <form id="logout" action="/logout" method="POST">{{csrf_field()}}</form>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- header top menu end -->