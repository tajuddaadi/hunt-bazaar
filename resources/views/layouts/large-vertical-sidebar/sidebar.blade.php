<div class="side-content-wrap">
    <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
        <ul class="navigation-left">
            <li class="nav-item {{ request()->is('admin.invitation/*') || request()->is('/') ? 'active' : '' }}">
                <a class="nav-item-hold" href="{{ route('admin.invitation.index') }}">
                    <i class="nav-icon i-Safe-Box1"></i>
                    <span class="nav-text">Dashboard</span>
                </a>
                <div class="triangle"></div>
            </li>
        </ul>
    </div>

    <div class="sidebar-overlay"></div>
</div>
<!--=============== Left side End ================-->