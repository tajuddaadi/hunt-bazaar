<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Hunt Bazaar</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="{{ asset('majestic-master/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('majestic-master/vendors/base/vendor.bundle.base.css') }}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <link rel="stylesheet" href="{{ asset('majestic-master/vendors/datatables.net-bs4/dataTables.bootstrap4.css') }}">
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{ asset('majestic-master/css/style.css') }}">
    <!-- endinject -->
    <meta name="keywords" content="Hunt Bazaar" />
    <meta name="description" content="Hunt Bazaar">
    <meta name="author" content="Tajudda">
    @yield('css')
</head>

<body>

    @yield('content')

    <footer class="footer">
        <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block">Copyright © 2021 <a href=""
                    target="_blank">Hunt Bazaar</a>. All rights reserved.</span>
        </div>
    </footer>
    <!-- partial -->
    </div>
    <!-- main-panel ends -->
    </div>
    <!-- page-body-wrapper ends -->
    </div>
    <!-- container-scroller -->

    <!-- plugins:js -->
    <script src="{{ asset('majestic-master/vendors/base/vendor.bundle.base.js') }}"></script>
    <!-- endinject -->
    <!-- Plugin js for this page-->
    <script src="{{ asset('majestic-master/vendors/chart.js/Chart.min.js') }}"></script>
    <script src="{{ asset('majestic-master/vendors/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('majestic-master/vendors/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <!-- End plugin js for this page-->
    <!-- inject:js -->
    <script src="{{ asset('majestic-master/js/off-canvas.js') }}"></script>
    <script src="{{ asset('majestic-master/js/hoverable-collapse.js') }}"></script>
    <script src="{{ asset('majestic-master/js/templateku.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page-->
    <script src="{{ asset('majestic-master/js/dashboard.js') }}"></script>
    <script src="{{ asset('majestic-master/js/data-table.js') }}"></script>
    <script src="{{ asset('majestic-master/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('majestic-master/js/dataTables.bootstrap4.js') }}"></script>
    <!-- End custom js for this page-->
    @if (session('status'))
    <script>
    alert("{{ session('status') }}");
    </script>
    @endif

    @yield('js')
</body>

</html>