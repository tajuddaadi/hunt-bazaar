<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        @auth("web")
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.invitation.index')}}">
                <i class="mdi mdi-view-headline menu-icon"></i>
                <span class="menu-title">Invitation</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('admin.entry.index')}}">
                <i class="mdi mdi-view-headline menu-icon"></i>
                <span class="menu-title">Entry</span>
            </a>
        </li>
        @endauth
    </ul>
</nav>