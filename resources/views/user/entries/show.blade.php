@extends('layouts.master')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/vendor/select2/css/select2.min.css') }}">
@endsection

@section('content')
<div class="container-scroller">
    @include('layouts.role.role-navbar')

    <!-- partial -->
    <div class="container-fluid page-body-wrapper">
        @include('layouts.role.role-sidebar')
        <div class="main-panel" style="overflow-y: scroll; height:450px;">

            <div class="content-wrapper">
                @if($checkInvitation == 0)
                <div class="row">
                    <div class="col-md-12 stretch-card">
                        <div class="card">
                            <div class="card-body" style="min-height: 600px;">
                                <template>
                                    @{{ timerCount }}
                                </template>
                                <h4 class="card-title">This invitation is for {{$invitation->email}}</h4>
                                @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                                @endif
                                @if (session('message'))
                                <div class="alert {{ session('class') }}">
                                    {{ session('message') }}
                                </div>
                                @endif
                                <p class="card-description">
                                    Please fill out the form below
                                </p>
                                <form method="POST" action="{{ route('entry.store') }}">
                                    @csrf
                                    <input type="hidden" name="invitation_id" value="{{$invitation->id}}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {!!Form::text('email', 'Email')->value($invitation->email)
                                            ->placeholder('InputEmail')->type('email')->readonly()->required() !!}
                                        </div>

                                        <div class="col-md-12">
                                            {!!Form::text('birth_date', 'Birth Date')
                                            ->placeholder('Input Date')->type('date') !!}
                                        </div>

                                        <div class="col-md-12">
                                            {!!Form::select('gender', 'Choose your gender', [1 => 'Male', 2 => 'Female',
                                            3 => 'Others'])->placeholder('Choose your gender')->required() !!}
                                        </div>

                                        <div class="col-md-12">
                                            <div class="form-group"><label for="">Favorite Designer</label>
                                                <select name="designer[]" class="form-control select2" required
                                                    data-placeholder="Search Designer" multiple="multiple">
                                                    @foreach($designers as $designer)
                                                    <option value="{{$designer->name}}">{{$designer->name}}</option>
                                                    @endforeach
                                                </select>
                                                @error('designer[]')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                        </div>

                                        <div class="col-md-12 mt-5">
                                            <div class="col-md-6 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Register') }}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div> <!-- row -->
                @else
                <div class="row">
                    <div class="col-md-12 stretch-card">
                        <div class="card">
                            <div class="card-body" style="min-height: 600px;">
                                <h4 class="card-title">Congratulations!</h4>
                                <p class="card-description">
                                    You have been registered in this event. Here is your invitation code :
                                    {{$invitation->invitation_token}}
                                </p>
                            </div>
                        </div>
                    </div>

                </div>
                @endif
            </div> <!-- content-wrapper ends -->
            @endsection

            @section('js')
            <script src="https://code.jquery.com/jquery-3.6.0.js"></script>
            <script src="{{ asset('assets/vendor/select2/js/select2.full.min.js') }}"></script>
            <script>
            function getDesignerSelection() {
                $('.select2').select2({
                    width: 'resolve',
                    ajax: {
                        url: "/api/get-designer",
                        dataType: 'json',
                        delay: 250,
                        processResults: function(data) {
                            return {
                                results: $.map(data, function(item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }

                                })
                            };
                        },
                        cache: true
                    },
                    minimumInputLength: 1,
                    language: {
                        inputTooShort: function() {
                            return 'Silahkan masukkan minimal 1 karakter untuk mencari data';
                        }
                    }
                });

                $('.select2').select2();
            }

            $(function() {
                getDesignerSelection();
            });
            </script>
            @endsection