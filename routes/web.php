<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\EntryController as EntryAdminController;
use App\Http\Controllers\Admin\InvitationController;
use App\Http\Controllers\User\EntryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth']], function () {
    //Route admin
    Route::prefix('admin')->name('admin.')->group(function () {
        Route::resource('invitation', InvitationController::class);
        Route::resource('entry', EntryAdminController::class);
    });
});

Route::resource('entry', EntryController::class);

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');